//
// Created by jrehorka on 4.5.17.
//

#ifndef LIDAR_SHARED_LINE_H_H
#define LIDAR_SHARED_LINE_H_H

#include "point.h"

#include <list>
#include <iostream>
#include <cpplinq.hpp>
#include <regex>

namespace lidar_shared {

///
/// Structure that represents line with a deviance.
///
struct Line : public IPrimitive {

  static const std::regex GetPattern() {
	  return std::regex("l-(\\d+\\.?\\d*),(\\d+\\.?\\d*),(\\d+\\.?\\d*),(\\d+\\.?\\d*),(\\d+\\.?\\d*)");
  }

  Point beginning;
  Point end;

  /// Line's RMSE
  double rmse;

  double Length() {
	  auto a_side = this->beginning.getX() - this->end.getX();
	  auto b_side = this->beginning.getY() - this->end.getY();

	  return std::sqrt(a_side * a_side + b_side * b_side);
  }

  const std::string Serialize() const {
	  return "l-" +
		  std::to_string(this->beginning.distance) + "," +
		  std::to_string(this->beginning.angle) + "," +
		  std::to_string(this->end.distance) + "," +
		  std::to_string(this->end.angle) + "," +
		  std::to_string(this->rmse) + ";";
  }

  double CalculateRmse(std::list<lidar_shared::Point> &line_points) {
	  double squares_sum = 0;
	  double distance_to_line;

	  for (const auto point : line_points) {
		  distance_to_line = lidar_shared::Point::GetDistanceFromEndPointsLine(this->beginning, this->end, point);

		  squares_sum += std::pow(distance_to_line, 2);
	  }

	  return squares_sum / line_points.size();
  }

  /// https://en.wikipedia.org/wiki/Simple_linear_regression
  ///
  /// \param data_points
  /// \return
  static lidar_shared::Line SimpleRegression(std::list<lidar_shared::Point> &data_points) {
	  auto first_point = data_points.front();
	  auto last_point = data_points.back();

	  double s_x = 0;
	  double s_y = 0;
	  double s_xx = 0;
	  double s_xy = 0;

	  int x;
	  int y;
	  auto n = data_points.size();

	  if (n == 0) {
		  std::cerr << "Data set must not be empty" << std::endl;
		  return lidar_shared::Line();
	  }

	  for (const auto &point : data_points) {
		  x = point.getX();
		  y = point.getY();

		  s_x += x;
		  s_y += y;
		  s_xx += x * x;
		  s_xy += x * y;
	  }

	  auto b = (n * s_xy - s_x * s_y) / (n * s_xx - s_x * s_x);
	  auto a = (1.0f / n) * s_y - b * (1.0f / n) * s_x;
	  auto line_fn = [&a, &b](const int x_point) { return a + b * x_point; };

	  auto fitted_line = lidar_shared::Line();
	  fitted_line.beginning =
		  lidar_shared::Point::FromCartesian(first_point.getX(), (float) line_fn(first_point.getX()));
	  fitted_line.end = lidar_shared::Point::FromCartesian(last_point.getX(), (float) line_fn(last_point.getX()));

	  return fitted_line;
  }
};
}

#endif //LIDAR_SHARED_LINE_H_H
//
// Created by jrehorka on 6.4.17.
//
