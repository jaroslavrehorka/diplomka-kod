//
// Created by jrehorka on 4.5.17.
//

#ifndef LIDAR_SHARED_IPRIMITIVE_H
#define LIDAR_SHARED_IPRIMITIVE_H

#include <string>
#include <regex>

namespace lidar_shared {
struct IPrimitive {
  ///
  /// Creates serialized string, that can be used for
  /// outgoing communication.
  ///
  /// \return
  virtual const std::string Serialize() const = 0;
};
}

#endif //LIDAR_SHARED_IPRIMITIVE_H
