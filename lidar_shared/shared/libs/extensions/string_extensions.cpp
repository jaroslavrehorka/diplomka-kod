//
// Created by jrehorka on 14.5.17.
//

#include "string_extensions.h"

bool lidar_shared::StringExtensions::HasSuffix(const std::string &str, const std::string &suffix) {
	return str.size() >= suffix.size() &&
		str.compare(str.size() - suffix.size(), suffix.size(), suffix) == 0;
}
