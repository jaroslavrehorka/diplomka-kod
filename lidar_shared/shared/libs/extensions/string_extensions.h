//
// Created by jrehorka on 14.5.17.
//

#ifndef LIDAR_CONNECTOR_STRING_EXTENSIONS_H
#define LIDAR_CONNECTOR_STRING_EXTENSIONS_H

#include <string>

namespace lidar_shared {
class StringExtensions {
public:
  static bool HasSuffix(const std::string &str, const std::string &suffix);
};
}

#endif //LIDAR_CONNECTOR_STRING_EXTENSIONS_H
