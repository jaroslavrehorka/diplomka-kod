FILE_TO_ANALYZE = '../sample_scans/3Droom01-corridor.las';
SCALING_FACTOR = 0.25;
ED_TRANSFORM_SIZE = 50*SCALING_FACTOR;
KERNEL_RADIUS = 300*SCALING_FACTOR;

addpath('plane_estimation/');
addpath('libs/');

las=lasdata(FILE_TO_ANALYZE);
las.get_color();

%%
points = [las.x, las.y, las.z];
points_trans = points';

previous_cloud_size = 0;
index = 0;

figure();
xlabel('X(mm)')
ylabel('Y(mm)')
zlabel('Z(mm)')
hold on

%% Vyhledani rovin, aktualne se vyhleda jen jedna a cyklus se zastavi
while size(points_trans) >= 3    
    index = 1 + index;
    [B, P, inliers] = ransacfitplane(points_trans, 50, 0);
    selected_points = points_trans(:,inliers)';
    
    %ransacfitplane vraci spatne d u parametru roviny, musime prepocitat
    first_point = selected_points(1,:);
    B(4) = B(1)*first_point(1) + B(2)*first_point(2) + B(3)*first_point(3);
    
    % pripocteme i body ktere lezi pod rovinou - do hloubky 2*d
    B(4) = 2 * B(4);
    
    parallel_plane_to_B = [B(1), B(2), B(3), 0]';    
    points_trans(:,inliers) = [];
    
    points_above_plane_indices = find_points_between_planes(B, parallel_plane_to_B, points_trans);
    points_above_plane = points_trans(:,points_above_plane_indices)';
    %points_trans(:,points_above_plane_indices) = [];
    
    if(size(inliers) < 200)
        continue;
    end      
    
    if isequal(previous_cloud_size, size(points_trans))
        break;
    end
    
    previous_cloud_size = size(points_trans);
    
    hold on;    
    scatter3(selected_points(:,1), selected_points(:,2), selected_points(:,3),'filled');    
    scatter3(points_trans(1,:), points_trans(2,:), points_trans(3,:),'filled');
    hold off;
    
    %% Transformace
    V = pca(selected_points);
    A_transformed = selected_points*V; %V(:,1) is the direction of most variance
    points_above_transformed = points_above_plane*V;

    x = A_transformed(:,1)';
    y = A_transformed(:,2)';
    z = A_transformed(:,3)';

    x_above = points_above_transformed(:,1)';
    y_above = points_above_transformed(:,2)';
    z_above = points_above_transformed(:,3)';

    %% Hledani vhodnych ploch
    max_x = ceil(max(horzcat(x,x_above))*SCALING_FACTOR);
    max_y = ceil(max(horzcat(y,y_above))*SCALING_FACTOR);
    min_x = floor(min(horzcat(x,x_above))*SCALING_FACTOR);
    min_y = floor(min(horzcat(y,y_above))*SCALING_FACTOR);
    min_z = floor(min(horzcat(z,z_above))*SCALING_FACTOR);

    [img3] = carthesian_to_image(x,y,z,SCALING_FACTOR,max_x,max_y,min_x,min_y,min_z);
    [img3_points_above] = carthesian_to_image(x_above,y_above,z_above,SCALING_FACTOR,max_x,max_y,min_x,min_y,min_z);
              
    euler_transformed = dt(img3 <= 0) < ED_TRANSFORM_SIZE;
    euler_transformed_points_above = dt(img3_points_above <= 0) < ED_TRANSFORM_SIZE;
    euler_transformed_final = euler_transformed - euler_transformed_points_above;

    img_result = opening(euler_transformed_final, KERNEL_RADIUS);

    total_length = length(x);  
    valid_points = [];
    invalid_points = [];
    %scatter3(valid_points(:,1), valid_points(:,2), valid_points(:,3),'filled');
    for index = 1:total_length
        [img_x,img_y,img_z] = get_image_coordinates(x(index),y(index),z(index),min_x,min_y,min_z,SCALING_FACTOR);    
        if(is_point_in_image(img_result,img_x,img_y))
            valid_points=[valid_points;x(index) y(index) z(index) 1];
        else            
            invalid_points=[invalid_points;x(index) y(index) z(index) 1];            
        end
    end

    fprintf('Rovina id: %d, pocet bodu %d\n',index, size(inliers));
    fprintf('Vhodnych bodu: %d\n', size(valid_points));
    fprintf('Okolnich bodu: %d\n', size(invalid_points));

    %% Vykresleni 2D
    figure();
    hold on;
    if(~isempty(valid_points))
        scatter(valid_points(:,1),valid_points(:,2),'filled');
    end

    if(~isempty(invalid_points))
        scatter(invalid_points(:,1),invalid_points(:,2),'filled');
    end

    if(~isempty(points_above_transformed))
        scatter3(points_above_transformed(:,1), points_above_transformed(:,2), points_above_transformed(:,3),'filled');
    end

    hold off

    %% Prevod zpet do 3D
    if(isempty(valid_points))
        A_orig_rot = zeros(3);
    else
        A_orig_rot = [valid_points(:,1),valid_points(:,2),valid_points(:,3)] * inv(V);
    end

    figure();
    hold on
    scatter3(las.x, las.y, las.z);
    scatter3(A_orig_rot(:,1), A_orig_rot(:,2), A_orig_rot(:,3), 'filled');
    scatter3(points_above_plane(:,1), points_above_plane(:,2), points_above_plane(:,3), 'filled');
    hold off

    disp('Press a key !')
    pause;
    close all;
end




