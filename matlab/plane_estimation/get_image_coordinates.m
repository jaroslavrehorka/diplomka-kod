function [img_x,img_y,img_z] = get_image_coordinates(x,y,z,min_x,min_y,min_z,zoom_factor)
    img_x = ceil(x*zoom_factor) - min_x;
    img_y = ceil(y*zoom_factor) - min_y;
    img_z = ceil(z) + abs(min_z);
end