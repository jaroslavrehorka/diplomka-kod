function is_point_in_img = is_point_in_image(bin_img,img_x,img_y)
    is_point_in_img = (bin_img(img_x,img_y) == 1);
end