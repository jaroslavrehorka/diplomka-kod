//
// Created by jrehorka on 3/19/17.
//

#ifndef LIDAR_CONNECTOR_OPTIONS_H_H
#define LIDAR_CONNECTOR_OPTIONS_H_H

#include <string>

namespace lidar_visualizer {
/// This structure is a container for application settings.
///
/// It holds all possible settings that can be passed to the
/// program through command line parameters.
struct Options {

  /// Flag that indicates if an extra output should be printed
  /// to the standard output.
  ///
  /// Default value is false.
  bool verbose;

  /// Flag that indicates if all output from the program should
  /// be muted.
  ///
  /// Has higher priority than verbose flag.
  ///
  /// Default value is false.
  bool quiet;

  /// Sockets data source address, default value is localhost.
  std::string sockets_server_address;

  /// Sockets data source port.
  std::string sockets_server_port;

  Options()
	  : verbose(false),
	    quiet(false),
	    sockets_server_address("localhost") {}
};
}

#endif //LIDAR_CONNECTOR_OPTIONS_H_H
