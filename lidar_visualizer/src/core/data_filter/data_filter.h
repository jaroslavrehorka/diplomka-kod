//
// Created by jrehorka on 1.5.17.
//

#ifndef LIDAR_VISUALIZER_DATA_FILTER_H
#define LIDAR_VISUALIZER_DATA_FILTER_H

#include "shared/model/point.h"
#include "model/filter.h"
#include "shared/model/line.h"

#include <vector>

namespace lidar_visualizer {
class DataFilter {
public:
  static const std::vector<lidar_shared::Point> ApplyFilter(std::vector<lidar_shared::Point> &points_vector,
                                                                lidar_visualizer::Filter &filter_options);

  static const std::vector<lidar_shared::Line> ApplyFilter(std::vector<lidar_shared::Line> &lines_vector,
                                                               lidar_visualizer::Filter &filter_options);
};
}

#endif //LIDAR_VISUALIZER_DATA_FILTER_H
