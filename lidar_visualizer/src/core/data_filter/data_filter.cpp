//
// Created by jrehorka on 1.5.17.
//

#include <cpplinq.hpp>
#include "data_filter.h"
const std::vector<lidar_shared::Point> lidar_visualizer::DataFilter::ApplyFilter(std::vector<lidar_shared::Point> &points_vector,
                                                                                 lidar_visualizer::Filter &filter_options) {

	auto filtered_points = cpplinq::from(points_vector);

	if (!filter_options.point_visible) {
		return filtered_points >> cpplinq::take(0) >> cpplinq::to_vector();
	}

	return filtered_points >> cpplinq::where([&filter_options](lidar_shared::Point point) {
	  if (filter_options.points_min_quality > -1 && point.quality < filter_options.points_min_quality) {
		  return false;
	  }

	  return true;
	})
	                       >> cpplinq::to_vector();
}
const std::vector<lidar_shared::Line> lidar_visualizer::DataFilter::ApplyFilter(std::vector<lidar_shared::Line> &lines_vector,
                                                                                lidar_visualizer::Filter &filter_options) {
	auto filtered_lines = cpplinq::from(lines_vector);

	if (!filter_options.lines_visible) {
		return filtered_lines >> cpplinq::take(0) >> cpplinq::to_vector();
	}

	return filtered_lines >> cpplinq::where([&filter_options](lidar_shared::Line line) {

	  if (filter_options.lines_min_length > -1 && line.Length() < filter_options.lines_min_length) {
		  return false;
	  }
	  if (filter_options.lines_max_length > -1 && line.Length() > filter_options.lines_max_length) {
		  return false;
	  }
	  if (filter_options.lines_max_rmse > -1
		  && line.rmse > filter_options.lines_max_rmse) {
		  return false;
	  }

	  return true;
	}) >> cpplinq::to_vector();
}
