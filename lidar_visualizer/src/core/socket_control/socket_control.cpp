//
// Created by jrehorka on 3/18/17.
//

#include <vector>
#include <iostream>
#include "socket_control.h"

lidar_visualizer::SocketControl::SocketControl(lidar_visualizer::Options &options)
	: options(options),
	  inet_client(libsocket::inet_stream()) {
}

lidar_visualizer::SocketControl::~SocketControl() {
	this->is_running = false;
	if (this->incomming_data_thread.joinable()) {
		this->incomming_data_thread.join();
	}

	this->inet_client << "disconnect";
	this->inet_client.shutdown();
	this->inet_client.destroy();
}

void lidar_visualizer::SocketControl::Connect() {
	if (!this->options.quiet) {
		std::cout << "Connecting to " << this->options.sockets_server_address << ":"
		          << this->options.sockets_server_port << std::endl;
		std::cout.flush();
	}

	this->inet_client.connect(this->options.sockets_server_address,
	                          this->options.sockets_server_port,
	                          LIBSOCKET_IPv4);

	std::cout << "Successfully connected!" << std::endl;

	this->incomming_data_thread = std::thread([=] { this->ReadIncommingData(); });
}

void lidar_visualizer::SocketControl::ReadData(std::vector<lidar_shared::Point> *points,
                                               std::vector<lidar_shared::Line> *lines) {
	this->inet_client.rcv(this->buffer, this->kDataBufferSize);

	std::stringstream scan_data(this->buffer);
	std::string token;
	std::smatch base_match;

	std::unique_lock<std::mutex> lock(this->data_access_mutex);

	points->clear();
	lines->clear();

	auto line_pattern = lidar_shared::Line::GetPattern();
	auto point_pattern = lidar_shared::Point::GetPattern();

	while (getline(scan_data, token, this->kPointDelimiter)) {
		if (std::regex_match(token, base_match, line_pattern)) {
			lidar_shared::Line line;
			line.beginning.distance = (float) ::atof(std::string(base_match[1].str()).c_str()) / 10;
			line.beginning.angle = (float) ::atof(std::string(base_match[2].str()).c_str());
			line.end.distance = (float) ::atof(std::string(base_match[3].str()).c_str()) / 10;
			line.end.angle = (float) ::atof(std::string(base_match[4].str()).c_str());
			line.rmse = (float) ::atof(std::string(base_match[5].str()).c_str());
			lines->push_back(line);

		} else if (std::regex_match(token, point_pattern)) {
			points->push_back(this->ParsePointString(token));
		}
	}
}

lidar_shared::Point lidar_visualizer::SocketControl::ParsePointString(const std::string &point_string) {
	lidar_shared::Point parsedPoint;
	constexpr int kMessagePartsCount = 3;

	std::istringstream is(point_string);
	std::string parts[kMessagePartsCount];
	std::string token;

	int i = 0;
	while (getline(is, token, this->kPointAttributesDelimiter)) {
		parts[i] = token;
		++i;
	}

	if (i == kMessagePartsCount) {
		parsedPoint.angle = (float) ::atof(parts[2].c_str());
		parsedPoint.distance = ((float) ::atof(parts[1].c_str())) / this->kDefaultDistanceScaling;
		parsedPoint.quality = (float) ::atof(parts[0].c_str());
	} else {
		parsedPoint.ClearPoint();
	}

	return parsedPoint;
}

void lidar_visualizer::SocketControl::ReadIncommingData() {
	while (true) {
		if (!this->is_running) {
			return;
		}

		this->ReadData(&points, &lines);

		this->has_new_data = true;
	}
}