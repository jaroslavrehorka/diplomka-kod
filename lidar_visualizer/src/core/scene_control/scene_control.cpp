//
// Created by jrehorka on 3/31/17.
//

#include "scene_control.h"
#include "rts_camera.h"
#include "scene_elements_enum.h"

#include <thread>

lidar_visualizer::SceneControl::~SceneControl() {
	device->drop();
}

void lidar_visualizer::SceneControl::InitDevice() {
	this->event_receiver.open_settings_window = [&]() {
	  this->CreateFilterWindow();
	};
	this->device =
		irr::createDevice(irr::video::EDT_OPENGL,
		                  irr::core::dimension2d<irr::u32>(1024, 768), 32,
		                  false, false, false, &this->event_receiver);

	if (!this->device) {
		throw std::runtime_error("Unable to create a device.");
	}

	this->device->setWindowCaption(L"Lidar Visualizer");

	this->videoDriver = device->getVideoDriver();
	this->sceneManager = device->getSceneManager();
	this->guiEnvironment = device->getGUIEnvironment();
}

void lidar_visualizer::SceneControl::InitScene() {

	auto *camera =
		new RTSCamera(device, this->sceneManager->getRootSceneNode(), this->sceneManager, -1, 100.0f, 10.0f, 10.0f);
	camera->setPosition(vector3df(0, 0, -500));
	camera->setTarget(vector3df(0, 0, 0));
	camera->setTranslateSpeed(5);
	camera->setRotationSpeed(50);

	this->guiEnvironment->addButton(core::rect<s32>(10, 10, 85, 30), 0, GUI_MENU_FILTERS_BUTTON, L"Filters");

	this->sceneManager->addLightSceneNode(0, irr::core::vector3df(-15, 5, -105),
	                                      irr::video::SColorf(1.0f, 1.0f, 1.0f));
	// set ambient light
	this->sceneManager->setAmbientLight(irr::video::SColor(0, 255, 255, 255));
	this->CreateDefaultTextures();

	auto window_height = this->videoDriver->getScreenSize().Height;
	this->info_box = this->guiEnvironment->addStaticText(L"",
	                                                     rect<s32>(10, window_height - 40, 200, window_height - 10),
	                                                     true,
	                                                     true,
	                                                     0,
	                                                     -1,
	                                                     true);
	this->info_box->setOverrideColor(irr::video::SColor(255, 255, 255, 255));
}

void lidar_visualizer::SceneControl::CreateDefaultTextures() {
	const irr::u32 width = 256;
	const irr::u32 height = 256;
	irr::video::IImage
		*imageA8R8G8B8 =
		this->videoDriver->createImage(irr::video::ECF_A8R8G8B8, irr::core::dimension2d<irr::u32>(width, height));
	if (!imageA8R8G8B8) {
		return;
	}

	imageA8R8G8B8->fill(SCOL_WHITE);
	this->videoDriver->addTexture(irr::io::path("WHITE_A8R8G8B8"), imageA8R8G8B8);

	imageA8R8G8B8->fill(SCOL_CYAN);
	this->videoDriver->addTexture(irr::io::path("CYAN_A8R8G8B8"), imageA8R8G8B8);

	imageA8R8G8B8->fill(SCOL_GREEN);
	this->videoDriver->addTexture(irr::io::path("GREEN_A8R8G8B8"), imageA8R8G8B8);

	imageA8R8G8B8->fill(SCOL_MAGENTA);
	this->videoDriver->addTexture(irr::io::path("MAGENTA_A8R8G8B8"), imageA8R8G8B8);

	imageA8R8G8B8->fill(SCOL_RED);
	this->videoDriver->addTexture(irr::io::path("RED_A8R8G8B8"), imageA8R8G8B8);

	imageA8R8G8B8->fill(SCOL_YELLOW);
	this->videoDriver->addTexture(irr::io::path("YELLOW_A8R8G8B8"), imageA8R8G8B8);

	imageA8R8G8B8->fill(SCOL_BLUE);
	this->videoDriver->addTexture(irr::io::path("BLUE_A8R8G8B8"), imageA8R8G8B8);

	imageA8R8G8B8->fill(SCOL_BLACK);
	this->videoDriver->addTexture(irr::io::path("BLACK_A8R8G8B8"), imageA8R8G8B8);

	imageA8R8G8B8->drop();
}

lidar_visualizer::SceneControl::SceneControl() {
	this->InitDevice();
	this->InitScene();
}

irr::scene::IMeshSceneNode *lidar_visualizer::SceneControl::CreatePoint(int x, int y, int z, float radius) {
	irr::core::vector3df position(x, y, z);
	irr::scene::IMeshSceneNode *sphere = this->sceneManager->addSphereSceneNode(radius);
	sphere->setPosition(position);
	sphere->setMaterialTexture(0, this->videoDriver->getTexture(irr::io::path("RED_A8R8G8B8")));
	sphere->setMaterialFlag(irr::video::EMF_LIGHTING, true);

	this->nodes.push_back(sphere);

	return sphere;
}

void *lidar_visualizer::SceneControl::CreateLine(lidar_shared::Line line) {
	auto start = irr::core::vector3df(line.beginning.getX(), line.beginning.getY(), line.beginning.getZ());
	auto end = irr::core::vector3df(line.end.getX(), line.end.getY(), line.end.getZ());
	auto material = irr::video::SMaterial();
	auto lineTexturePath = irr::io::path("GREEN_A8R8G8B8");

	if (this->event_receiver.filters->lines_rmse_colored) {
		if (line.rmse < 2) {
			lineTexturePath = irr::io::path("YELLOW_A8R8G8B8");
		} else if (line.rmse < 5) {
			lineTexturePath = irr::io::path("GREEN_A8R8G8B8");
		} else if (line.rmse < 10) {
			lineTexturePath = irr::io::path("CYAN_A8R8G8B8");
		} else if (line.rmse < 20) {
			lineTexturePath = irr::io::path("BLUE_A8R8G8B8");
		} else {
			lineTexturePath = irr::io::path("MAGENTA_A8R8G8B8");
		}
	}

	material.setTexture(0, this->videoDriver->getTexture(lineTexturePath));
	videoDriver->setMaterial(material);
	videoDriver->setTransform(irr::video::ETS_WORLD, irr::core::IdentityMatrix);
	videoDriver->draw3DLine(start, end, irr::video::SColor(255, 0, 255, 0));
}

void lidar_visualizer::SceneControl::ClearScene() {
	std::for_each(this->nodes.begin(), this->nodes.end(), [](irr::scene::IMeshSceneNode *item) {
	  item->remove();
	});
	this->nodes.clear();
}

bool lidar_visualizer::SceneControl::IsRunning() {
	return device->run();
}

void lidar_visualizer::SceneControl::BeginDrawing() {
	this->videoDriver->beginScene(true, true, irr::video::SColor(0, 0, 0, 0));
}

void lidar_visualizer::SceneControl::EndDrawing() {
	this->VizualizeDevice();
	this->sceneManager->drawAll();
	this->guiEnvironment->drawAll();

	this->videoDriver->endScene();
}
void lidar_visualizer::SceneControl::UpdateInfoBox(std::wstring text) {
	auto cam = this->sceneManager->getActiveCamera();
	std::wstring camera_pos =
		L"\nx: " + std::to_wstring(cam->getPosition().X) + L" Y: " + std::to_wstring(cam->getPosition().Y) + L" Z: "
			+ std::to_wstring(cam->getPosition().Z);
	this->info_box->setText((text + camera_pos).c_str());
}

void lidar_visualizer::SceneControl::CreateFilterWindow() {
// remove tool box if already there
	IGUIEnvironment *env = this->device->getGUIEnvironment();
	IGUIElement *root = env->getRootGUIElement();
	IGUIElement *e = root->getElementFromId(GUI_ID_DIALOG_ROOT_WINDOW, true);
	if (e) {
		e->remove();
	}

	// create the toolbox window
	IGUIWindow *wnd = env->addWindow(core::rect<s32>(10, 45, 210, 300),
	                                 false, L"Filters", 0, GUI_ID_DIALOG_ROOT_WINDOW);

	// add some edit boxes and a button to tab one
	env->addStaticText(L"Visibility:",
	                   core::rect<s32>(10, 20, 60, 45), false, false, wnd);
	env->addStaticText(L"Points:", core::rect<s32>(22, 48, 40, 66), false, false, wnd);
	env->addCheckBox(this->event_receiver.filters->point_visible,
	                 core::rect<s32>(110, 46, 130, 66),
	                 wnd,
	                 GUI_POINTS_VISIBILITY);
	env->addStaticText(L"Lines:", core::rect<s32>(22, 82, 40, 96), false, false, wnd);
	env->addCheckBox(this->event_receiver.filters->lines_visible,
	                 core::rect<s32>(110, 76, 130, 96),
	                 wnd,
	                 GUI_LINES_VISIBILITY);

	env->addStaticText(L"Points min quality:", core::rect<s32>(22, 108, 105, 126), false, false, wnd);
	env->addEditBox(std::to_wstring(this->event_receiver.filters->points_min_quality).c_str(),
	                core::rect<s32>(110, 106, 160, 126),
	                true,
	                wnd,
	                GUI_POINTS_MIN_QUALITY);

	env->addStaticText(L"Lines max RMSE:", core::rect<s32>(22, 138, 105, 156), false, false, wnd);
	env->addEditBox(std::to_wstring(this->event_receiver.filters->lines_max_rmse).c_str(),
	                core::rect<s32>(110, 136, 160, 156),
	                true,
	                wnd,
	                GUI_LINES_MAX_RMSE);

	env->addStaticText(L"Colored RMSE:", core::rect<s32>(22, 168, 105, 186), false, false, wnd);
	env->addCheckBox(this->event_receiver.filters->lines_rmse_colored,
	                 core::rect<s32>(110, 166, 160, 186),
	                 wnd,
	                 GUI_LINES_COLORED);

	env->addStaticText(L"Lines min length (cm):", core::rect<s32>(22, 198, 105, 216), false, false, wnd);
	env->addEditBox(std::to_wstring(this->event_receiver.filters->lines_min_length).c_str(),
	                core::rect<s32>(110, 196, 160, 216),
	                true,
	                wnd,
	                GUI_POINTS_MIN_LENGTH);

	env->addStaticText(L"Lines max length (cm):", core::rect<s32>(22, 228, 105, 246), false, false, wnd);
	env->addEditBox(std::to_wstring(this->event_receiver.filters->lines_max_length).c_str(),
	                core::rect<s32>(110, 226, 160, 246),
	                true,
	                wnd,
	                GUI_POINTS_MAX_LENGTH);
}

void lidar_visualizer::SceneControl::VizualizeDevice() {
	auto device = this->CreatePoint(0, 0, 0, 3);
	device->setMaterialTexture(0, this->videoDriver->getTexture(irr::io::path("MAGENTA_A8R8G8B8")));

	auto zero_line_beginning = lidar_shared::Point();
	zero_line_beginning.angle = 0;
	zero_line_beginning.distance = 0;

	auto zero_line_end = lidar_shared::Point();
	zero_line_end.angle = 0;
	zero_line_end.distance = this->kZeroAngleLineSize;

	auto zero_angle_line = lidar_shared::Line();
	zero_angle_line.beginning = zero_line_beginning;
	zero_angle_line.end = zero_line_end;
	this->CreateLine(zero_angle_line);
}
