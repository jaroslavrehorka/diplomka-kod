//
// Created by jrehorka on 18.5.17.
//

#include "scene_event_receiver.h"

bool lidar_visualizer::SceneEventReceiver::OnEvent(const irr::SEvent &event) {
	if (event.EventType == irr::EET_GUI_EVENT) {
		auto id = event.GUIEvent.Caller->getID();

		switch (event.GUIEvent.EventType) {
		case irr::gui::EGET_EDITBOX_CHANGED: {
			auto caller = (irr::gui::IGUIEditBox *) event.GUIEvent.Caller;
			auto text = caller->getText() == L"" || caller->getText() == nullptr ? L"-1" : caller->getText();
			int parsed_number = -1;

			try {
				parsed_number = std::stoi(text);
			} catch (std::invalid_argument) {
			}

			switch (id) {
			case GUI_POINTS_MIN_QUALITY: this->filters->points_min_quality = parsed_number;
				break;
			case GUI_LINES_MAX_RMSE: this->filters->lines_max_rmse = parsed_number;
				break;
			case GUI_POINTS_MIN_LENGTH: this->filters->lines_min_length = parsed_number;
				break;
			case GUI_POINTS_MAX_LENGTH: this->filters->lines_max_length = parsed_number;
				break;
			default:break;
			}
		}
			break;
		case irr::gui::EGET_CHECKBOX_CHANGED: {
			auto caller_check_box = (irr::gui::IGUICheckBox *) event.GUIEvent.Caller;
			auto check_box_state = caller_check_box->isChecked();
			switch (id) {
			case GUI_POINTS_VISIBILITY: this->filters->point_visible = check_box_state;
				break;
			case GUI_LINES_VISIBILITY: this->filters->lines_visible = check_box_state;
				break;
			case GUI_LINES_COLORED: this->filters->lines_rmse_colored = check_box_state;
				break;
			default:break;
			}
		}
		case irr::gui::EGET_BUTTON_CLICKED: {
			switch (id) {
			case GUI_MENU_FILTERS_BUTTON: this->open_settings_window();
				break;
			default:break;
			}
		}
			break;
		default: break;
		}
	}

	return false;
}