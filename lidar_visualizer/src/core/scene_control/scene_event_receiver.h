//
// Created by jrehorka on 17.5.17.
//

#ifndef LIDAR_VISUALIZER_SCENE_EVENT_RECEIVER_H
#define LIDAR_VISUALIZER_SCENE_EVENT_RECEIVER_H

#include "scene_elements_enum.h"
#include "model/filter.h"

#include <IEventReceiver.h>
#include <iostream>
#include <IGUIEditBox.h>
#include <IGUICheckBox.h>
#include <functional>
#include <IGUIContextMenu.h>

namespace lidar_visualizer {
class SceneEventReceiver : public irr::IEventReceiver {
public:
  lidar_visualizer::Filter *filters;

  virtual bool OnEvent(const irr::SEvent &event);

  std::function<void()> open_settings_window;
};
}

#endif //LIDAR_VISUALIZER_SCENE_EVENT_RECEIVER_H
