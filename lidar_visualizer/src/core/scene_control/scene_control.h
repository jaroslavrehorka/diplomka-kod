//
// Created by jrehorka on 3/31/17.
//

#ifndef LIDAR_VISUALIZER_SCENE_CONTROL_H
#define LIDAR_VISUALIZER_SCENE_CONTROL_H

#include "shared/model/line.h"
#include "scene_elements_enum.h"
#include "scene_event_receiver.h"

#include <irrlicht.h>
#include <vector>
#include <model/filter.h>

namespace lidar_visualizer {
class SceneControl {
private:
  constexpr static int kZeroAngleLineSize = 100;

  const irr::video::SColor SCOL_BLACK = irr::video::SColor(255, 0, 0, 0);
  const irr::video::SColor SCOL_BLUE = irr::video::SColor(255, 0, 0, 255);
  const irr::video::SColor SCOL_CYAN = irr::video::SColor(255, 0, 255, 255);
  const irr::video::SColor SCOL_GRAY = irr::video::SColor(255, 128, 128, 128);
  const irr::video::SColor SCOL_GREEN = irr::video::SColor(255, 0, 255, 0);
  const irr::video::SColor SCOL_MAGENTA = irr::video::SColor(255, 255, 0, 255);
  const irr::video::SColor SCOL_RED = irr::video::SColor(255, 255, 0, 0);
  const irr::video::SColor SCOL_YELLOW = irr::video::SColor(255, 255, 255, 0);
  const irr::video::SColor SCOL_WHITE = irr::video::SColor(255, 255, 255, 255);

  irr::IrrlichtDevice *device;
  irr::video::IVideoDriver *videoDriver;
  irr::scene::ISceneManager *sceneManager;
  irr::gui::IGUIEnvironment *guiEnvironment;
  std::vector<irr::scene::IMeshSceneNode *> nodes;
  irr::gui::IGUIStaticText *info_box;

  /// Initializes program's window, scene manager, video driver and gui environment.
  void InitDevice();

  /// Initializes scene - loads materials, sets lights and cameras.
  void InitScene();

  /// Initializes defaut textures that are available for scene objects.
  void CreateDefaultTextures();

  void VizualizeDevice();

public:
  SceneControl();
  ~SceneControl();
  bool IsRunning();
  void BeginDrawing();
  void EndDrawing();
  void ClearScene();
  irr::scene::IMeshSceneNode *CreatePoint(int x, int y, int z, float radius = 0.4);
  void *CreateLine(lidar_shared::Line line);
  void UpdateInfoBox(std::wstring text);
  void CreateFilterWindow();
  SceneEventReceiver event_receiver;
};
}

#endif //LIDAR_VISUALIZER_SCENE_CONTROL_H
