//
// Created by jrehorka on 3/20/17.
//

#ifndef LIDAR_VISUALIZER_BOOTSTRAP_H
#define LIDAR_VISUALIZER_BOOTSTRAP_H

#include <core/scene_control/scene_event_receiver.h>
#include "model/filter.h"
#include "model/options.h"

namespace lidar_visualizer {

/// Main activation class that handles all required
/// activities for starting up the program.
class Bootstrap {
private:
  lidar_visualizer::Options run_options;
  lidar_visualizer::Filter data_filters;

  static bool ctrl_c_pressed;
  static void StopSignal(int);

public:
  Bootstrap(const lidar_visualizer::Options run_options);

  /// Main method that runs the program.
  int Run();
};
}

#endif //LIDAR_VISUALIZER_BOOTSTRAP_H
