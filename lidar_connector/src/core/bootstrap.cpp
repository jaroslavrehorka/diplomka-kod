//
// Created by jrehorka on 3/20/17.
//

#include "bootstrap.h"
#include "core/file_logger/las_logger/las_logger.h"
#include "core/socket_control/socket_control.h"
#include "driver/rp_lidar_driver.h"
#include "driver/text_file_driver.h"
#include "core/file_logger/file_logger.h"

#include <csignal>
#include <driver/exception/reading_exception.h>

bool lidar_connector::Bootstrap::ctrl_c_pressed = false;

void lidar_connector::Bootstrap::StopSignal(int) {
	ctrl_c_pressed = true;
}

lidar_connector::Bootstrap::Bootstrap(const lidar_connector::Options run_options)
	: run_options(run_options) {}

int lidar_connector::Bootstrap::Run() {

	auto return_status = EXIT_SUCCESS;

	std::unique_ptr<lidar_connector::IDriver> lidar_driver;
	std::ifstream file_data;

	if (this->run_options.read_from_file) {
		file_data.open(this->run_options.input_file);
		std::cout << this->run_options.input_file << std::endl;
		lidar_driver = std::make_unique<lidar_connector::TextFileDriver>(file_data, false, 0);
	} else {
		lidar_driver = std::make_unique<lidar_connector::RplidarDriver>();
	}

	auto socket_control = this->run_options.use_sockets ?
	                      std::make_unique<lidar_connector::SocketControl>(this->run_options)
	                                                    : nullptr;

	auto las_storage_control = this->run_options.log_to_file ?
	                           std::make_unique<lidar_connector::FileLogger>(this->run_options.log_file_name)
	                                                         : nullptr;

	try {
		lidar_driver->Connect();

		if (this->run_options.reset_device) {
			lidar_driver->Reset();

			if (!this->run_options.quiet) {
				std::cout << "The device was successfully reseted." << std::endl;
			}

			return EXIT_SUCCESS;
		}

		lidar_driver->StartScanning(this->run_options.motor_pwd);

		std::signal(SIGINT, lidar_connector::Bootstrap::StopSignal);
		auto iter = 0;

		do {
			auto data = lidar_driver->GetScanData(this->run_options.iterations_to_average);

			if (this->run_options.use_sockets) {
				socket_control->SendData(data);
			}

			if (!this->run_options.quiet) {
				std::cout << "Points detected: " << data.size() << std::endl;
			}

			for (auto const &value: data) {
				if (!this->run_options.quiet && this->run_options.verbose) {
					std::cout << value.GetString() << std::endl;
				}
			}

			std::cout << '\n' << "nth iter -- " << iter;
			getchar();

			if (this->run_options.log_to_file) {
				//for (auto &point : data) {
				//	point.z = this->z_value;
				//}
				las_storage_control->SaveIteration(data);

				//z_value += 50;
			}

			if (ctrl_c_pressed) {
				break;
			}

			++iter;
			//std::this_thread::sleep_for(std::chrono::seconds(1));
		} while (!this->run_options.single_scan);

	} catch (lidar_connector::ReadingException &er) {
		std::cout << er.what() << std::endl;
	} catch (std::exception &er) {
		std::cerr << er.what() << std::endl;
		return_status = EXIT_FAILURE;
	} catch (libsocket::socket_exception &er) {
		std::cerr << "Socket exception" << er.mesg << std::endl;
		return_status = EXIT_FAILURE;
	} catch (...) {
		std::cerr << "Unknown exception exception" << std::endl;
		return_status = EXIT_FAILURE;
	}

	lidar_driver->StopScanning();

	return return_status;
}
