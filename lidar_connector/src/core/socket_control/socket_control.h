//
// Created by jrehorka on 3/18/17.
//

#ifndef LIDAR_CONNECTOR_SOCKET_CONTROL_H
#define LIDAR_CONNECTOR_SOCKET_CONTROL_H

#include "shared/model/point.h"
#include "socket_client.h"

#include <string>

#include <inetserverdgram.hpp>
#include <vector>
#include <thread>
#include <inetserverstream.hpp>
#include <fstream>

namespace lidar_connector {

/// This class is a simplified wrapper of libsocket library
/// that is used for interprocess communication.
class SocketControl {
private:
  constexpr static char kPointDelimiter = ';';
  constexpr static char kPointAttributesDelimiter = ',';
  constexpr static uint32_t kBufferSize = 60000;
  std::string buffer;
  std::string communication_buffer;
  lidar_connector::Options &options;
  std::vector<lidar_connector::SocketClient> connected_clients;
  std::thread connection_thread;

  libsocket::inet_stream_server host_server;

public:
  /// Initializes SocketControl with given address and port.
  ///
  /// \param options program options with all settings neccessary to run
  /// \param host_port port to be used for socket host (eg. "1234").
  SocketControl(lidar_connector::Options &options);
  ~SocketControl();

  /// Blocking method that waits for an incoming connection,
  /// block is released when a new connection is estabilished.
  void WaitForRemoteConnections();

  /// Sends data to remotely connected client.
  ///
  /// \param data string data to be send.
  void SendData(const string &data);

  /// Sends data to remotely connected client.
  ///
  /// \see SendData(string).
  void SendData(const lidar_shared::Point &data);

  /// Sends data to remotely connected client.
  ///
  /// \see SendData(string).
  void SendData(const std::vector<lidar_shared::Point> &data);
};
}
#endif //LIDAR_CONNECTOR_SOCKET_CONTROL_H
