//
// Created by jrehorka on 3/18/17.
//

#include "model/options.h"
#include "socket_control.h"

#include <algorithm>
#include <select.hpp>

namespace lidar_connector {

SocketControl::SocketControl(lidar_connector::Options &options)
	: options(options),
	  host_server(libsocket::inet_stream_server(options.sockets_host_address,
	                                            options.sockets_host_port,
	                                            LIBSOCKET_IPv4)) {

	this->buffer.resize(this->kBufferSize);
	this->communication_buffer.resize(this->kBufferSize);
	this->connection_thread = std::thread([=] { this->WaitForRemoteConnections(); });
}

SocketControl::~SocketControl() {
	this->host_server.destroy();
}

void SocketControl::WaitForRemoteConnections() {
	libsocket::selectset<libsocket::inet_stream_server> set1;
	set1.add_fd(this->host_server, LIBSOCKET_READ);

	while (true) {
		lidar_connector::SocketClient new_client;
		libsocket::selectset<libsocket::inet_stream_server>::ready_socks readypair;

		readypair = set1.wait(); // Wait for a connection and save the pair to the var

		auto ready_srv =
			dynamic_cast<libsocket::inet_stream_server *>(readypair.first.back()); // Get the last fd of the LIBSOCKET_READ vector (.first) of the pair and cast the socket* to inet_stream_server*

		readypair.first.pop_back(); // delete the fd from the pair

		auto cl1 = ready_srv->accept2();

		new_client.remote_stream = std::move(cl1);

		std::cout << "Pripojen klient " << new_client.GetRemoteAddress() << ":" << new_client.GetRemotePort();
		this->connected_clients.push_back(std::move(new_client));
	}
}

void SocketControl::SendData(const std::string &data) {
	auto x = std::string(this->buffer);

	for (const lidar_connector::SocketClient &client : this->connected_clients) {
		if (client.remote_stream == nullptr) {
			continue;
		}

		try {
			(*client.remote_stream).snd(data.c_str(), data.length());
		} catch (libsocket::socket_exception &ex) {
			throw std::runtime_error(ex.mesg);
		}
	}
}

void SocketControl::SendData(const lidar_shared::Point &data) {
	this->SendData(data.Serialize());
}

void SocketControl::SendData(const std::vector<lidar_shared::Point> &data) {
	std::stringstream data_to_send;

	for (auto point : data) {
		data_to_send << point.Serialize();
	}

	this->SendData(data_to_send.str());
}
}
