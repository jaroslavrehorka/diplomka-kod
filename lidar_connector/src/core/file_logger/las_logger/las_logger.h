//
// Created by jrehorka on 3/21/17.
//

#ifndef LIDAR_CONNECTOR_LAS_CONTROL_H
#define LIDAR_CONNECTOR_LAS_CONTROL_H

#include "shared/model/point.h"

#include <string>
#include <laswriter.hpp>
#include "core/file_logger/Ifile_logger.h"

namespace lidar_connector {

/// This class controls saving lidar Points into LAS files.
class LasLogger : public IFileLogger {
private:
  std::string filename;
  U16 number_of_points = 0;
  LASwriteOpener las_opener;
  LASheader las_header;
  LASwriter *las_writer;

public:
  /// Initializes saving into file with name given in param.
  ///
  /// \param filename
  LasLogger(std::string filename);
  ~LasLogger();

  /// Stores the point into the LAS file.
  ///
  /// \param point
  void SavePoint(lidar_shared::Point point);

  /// Stores one scan iteration into las file.
  ///
  /// \param scan_iteration
  void SaveIteration(const std::vector<lidar_shared::Point> scan_iteration);
};
}

#endif //LIDAR_CONNECTOR_LAS_CONTROL_H
