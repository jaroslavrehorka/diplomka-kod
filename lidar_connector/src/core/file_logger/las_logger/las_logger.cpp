//
// Created by jrehorka on 3/21/17.
//

#include "las_logger.h"

#include <laswriter.hpp>
#include <iostream>

lidar_connector::LasLogger::LasLogger(std::string filename) {

	this->las_opener.set_file_name(filename.c_str());

	if (!this->las_opener.active()) {
		throw std::runtime_error("Cannot create las file " + filename);
	}

	this->las_header.x_scale_factor = 1;
	this->las_header.y_scale_factor = 1;
	this->las_header.z_scale_factor = 1;
	this->las_header.x_offset = 0;
	this->las_header.y_offset = 0;
	this->las_header.z_offset = 0;
	this->las_header.point_data_format = 3;
	this->las_header.point_data_record_length = 34;
	strcpy(this->las_header.generating_software, "LidarConnector");

	this->las_writer = this->las_opener.open(&this->las_header);
	if (this->las_writer == 0) {
		throw std::runtime_error("Cannot open las file " + filename + " for writing.");
	};
}

lidar_connector::LasLogger::~LasLogger() {
	if (this->las_writer == nullptr) {
		return;
	}

	this->las_header.number_of_point_records = this->number_of_points;
	this->las_writer->update_header(&this->las_header, TRUE);
	this->las_writer->close();

	delete (this->las_writer);
}

void lidar_connector::LasLogger::SavePoint(const lidar_shared::Point point) {
	if (point.quality != 0) {
		LASpoint laspoint;
		laspoint.init(
			&this->las_header,
			this->las_header.point_data_format,
			this->las_header.point_data_record_length,
			0
		);

		laspoint.set_X(point.getX());
		laspoint.set_Y(point.getY());
		laspoint.set_Z(point.getZ());
		laspoint.set_intensity((U16) 1);
		laspoint.set_classification(1);

		this->number_of_points++;
		this->las_writer->write_point(&laspoint);
		this->las_writer->update_inventory(&laspoint);
	}
}

void lidar_connector::LasLogger::SaveIteration(const std::vector<lidar_shared::Point> scan_iteration) {
	for (const auto &point: scan_iteration) {
		this->SavePoint(point);
	}
}
