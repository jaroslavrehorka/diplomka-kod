//
// Created by jrehorka on 9.5.17.
//

#include "plain_text_logger.h"

#include <fstream>

lidar_connector::PlainTextLogger::PlainTextLogger(std::string filename) {
	this->opened_stream = std::ofstream(filename, std::ios_base::app | std::ios_base::out);
}

lidar_connector::PlainTextLogger::~PlainTextLogger() {
	this->opened_stream.close();
}

void lidar_connector::PlainTextLogger::SaveIteration(const std::vector<lidar_shared::Point> scan_iteration) {
	for (const auto &point: scan_iteration) {
		this->opened_stream << point.Serialize();
	}

	this->opened_stream << '\n';
}
