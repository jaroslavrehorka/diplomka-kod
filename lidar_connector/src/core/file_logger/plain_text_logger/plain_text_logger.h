//
// Created by jrehorka on 9.5.17.
//

#ifndef LIDAR_CONNECTOR_PLAIN_TEXT_LOGGER_H
#define LIDAR_CONNECTOR_PLAIN_TEXT_LOGGER_H

#include "core/file_logger/Ifile_logger.h"
#include <fstream>

namespace lidar_connector {
class PlainTextLogger : public IFileLogger {
private:
  std::basic_ofstream<char, std::char_traits<char>> opened_stream;
public:
  /// Initializes saving into file with name given in param.
  ///
  /// \param filename
  PlainTextLogger(std::string filename);
  ~PlainTextLogger();

  /// Stores one scan iteration into text file.
  ///
  /// \param scan_iteration
  void SaveIteration(const std::vector<lidar_shared::Point> scan_iteration);
};
}

#endif //LIDAR_CONNECTOR_PLAIN_TEXT_LOGGER_H
