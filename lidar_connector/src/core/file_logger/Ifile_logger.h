//
// Created by jrehorka on 9.5.17.
//

#ifndef LIDAR_CONNECTOR_IFILE_LOGGER_H
#define LIDAR_CONNECTOR_IFILE_LOGGER_H

#include "shared/model/point.h"
#include <vector>

namespace lidar_connector {
class IFileLogger {
public:
  virtual void SaveIteration(const std::vector<lidar_shared::Point> scan_iteration) = 0;
  virtual ~IFileLogger() {};
};
}

#endif //LIDAR_CONNECTOR_IFILE_LOGGER_H
