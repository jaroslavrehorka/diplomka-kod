//
// Created by jrehorka on 3/20/17.
//

#ifndef LIDAR_CONNECTOR_BOOTSTRAP_H
#define LIDAR_CONNECTOR_BOOTSTRAP_H

#include "model/options.h"

namespace lidar_connector {

/// Main activation class that handles all required
/// activities for starting up the program.
class Bootstrap {
private:
  lidar_connector::Options run_options;
  int z_value = 0;
  static bool ctrl_c_pressed;
  static void StopSignal(int);

public:
  Bootstrap(const lidar_connector::Options run_options);

  /// Main method that runs the program.
  int Run();
};
}

#endif //LIDAR_CONNECTOR_BOOTSTRAP_H
