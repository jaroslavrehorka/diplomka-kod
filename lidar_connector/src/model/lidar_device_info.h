#ifndef LIDAR_CONNECTOR_LIDARDEVICEINFO_H
#define LIDAR_CONNECTOR_LIDARDEVICEINFO_H

#include <string>

namespace lidar_connector {

/// This class holds infomations about a LIDAR device.
struct lidar_device_info {
  std::string model_name_;
  std::string manufacturer_;
  std::string default_port_;
  uint32_t default_baudrate_;
  double firmware_version_;
  double hardware_version_;
  std::string serial_number_;
};
}

#endif //LIDAR_CONNECTOR_LIDARDEVICEINFO_H
