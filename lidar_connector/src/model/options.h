//
// Created by jrehorka on 3/19/17.
//

#ifndef LIDAR_CONNECTOR_OPTIONS_H_H
#define LIDAR_CONNECTOR_OPTIONS_H_H

#include <string>

namespace lidar_connector {
/// This structure is a container for application settings.
///
/// It holds all possible settings that can be passed to the
/// program through command line parameters.
struct Options {

  /// If set LIDAR run just one scan cycle and then shuts down.
  ///
  /// Default value is false.
  bool single_scan;

  /// Flag that indicates if an extra output should be printed
  /// to the standard output.
  ///
  /// Default value is false.
  bool verbose;

  /// Flag that indicates if all output from the program should
  /// be muted.
  ///
  /// Has higher priority than verbose flag.
  ///
  /// Default value is false.
  bool quiet;

  /// COM port of a lidar device.
  std::string lidar_port;

  /// Flag that indicates whether sockets should be used.
  ///
  /// Default value is false.
  bool use_sockets;

  /// Sockets host address, default value is localhost.
  std::string sockets_host_address;

  /// Sockets host port.
  std::string sockets_host_port;

  /// Flag that indicates whether lidar data should be saved
  /// into LAS file.
  ///
  /// Default value is false.
  bool log_to_file;

  /// LOG filename.
  std::string log_file_name;

  /// Input file name.
  std::string input_file;

  /// Flag that indicates whether lidar data should be readed from
  /// a file.
  ///
  /// Default value is false.
  bool read_from_file;

  /// PWD rotating speed.
  uint16_t motor_pwd;

  /// Flag that forces device to reset its state.
  bool reset_device;

  /// Number of scans that will be used in moving average.
  int iterations_to_average;

  Options()
	  : single_scan(false),
	    verbose(false),
	    quiet(false),
	    use_sockets(false),
	    log_to_file(false),
	    reset_device(false),
	    motor_pwd(600),
	    sockets_host_address("localhost"),
	    read_from_file(false),
	    iterations_to_average(0) {}
};
}

#endif //LIDAR_CONNECTOR_OPTIONS_H_H
