//
// Created by jrehorka on 4/4/17.
//

#include "text_file_driver.h"
#include "driver/exception/reading_exception.h"

#include <fstream>
#include <iostream>
#include <unistd.h>
#include <mydefs.hpp>
#include <laswriter.hpp>
#include <lasreader.hpp>

lidar_connector::TextFileDriver::TextFileDriver(std::ifstream &file_data_stream,
                                                bool loop_file,
                                                uint32_t simulated_delay_seconds)
	: simulated_delay_seconds(simulated_delay_seconds),
	  infile(file_data_stream),
	  rewind_on_finish(loop_file) {
}

void lidar_connector::TextFileDriver::Connect() {
	return;
}

const lidar_connector::lidar_device_info lidar_connector::TextFileDriver::GetDeviceInfo() const {
	auto device_info = lidar_connector::lidar_device_info();
	device_info.default_baudrate_ = 0;
	device_info.default_port_ = "0";
	device_info.firmware_version_ = 0;
	device_info.hardware_version_ = 0;
	device_info.manufacturer_ = "0";
	device_info.model_name_ = "Plain Text File Reader";
	device_info.serial_number_ = "0";

	return device_info;
}

const bool lidar_connector::TextFileDriver::IsConnected() const {
	return true;
}

void lidar_connector::TextFileDriver::StartScanning(uint16_t motor_pwd) {
	return;
}

void lidar_connector::TextFileDriver::StopScanning() {
	return;
}

std::vector<lidar_shared::Point> lidar_connector::TextFileDriver::GetScanData(int iterations_to_average) {
	std::string iteration_line;
	auto points_vector = std::vector<lidar_shared::Point>();

	if (this->infile >> iteration_line) {
		this->ParsePointsLine(iteration_line, &points_vector);
	}

	if (this->infile.eof() && rewind_on_finish) {
		this->infile.clear();
		this->infile.seekg(0, std::ios::beg);
	} else if (this->infile.eof()) {
		throw lidar_connector::ReadingException("EOF reached.");
	}

	sleep(this->simulated_delay_seconds);

	if (iterations_to_average < 2) {
		return std::move(points_vector);
	}

	this->past_scans.push_back(points_vector);

	if (this->past_scans.size() <= iterations_to_average) {
		this->GetScanData(iterations_to_average);
		return points_vector;
	}

	this->past_scans.pop_front();
	return std::move(this->GetAverageScan());
}

std::vector<lidar_shared::Point> lidar_connector::TextFileDriver::GetAverageScan() const {
	auto average_scan = std::vector<lidar_shared::Point>();

	for (int i = 0; i < kScannedAngle; ++i) {
		auto point = lidar_shared::Point();
		int points_used = 0;
		point.ClearPoint();

		for (auto vect : this->past_scans) {
			if (vect[i].quality == 0) {
				continue;
			}

			point.quality += vect[i].quality;
			point.distance += vect[i].distance;
			point.angle += vect[i].angle;
			++points_used;
		}

		if (points_used == 0) {
			++points_used;
		}

		point.quality /= points_used;
		point.distance /= points_used;
		point.angle /= points_used;
		average_scan.push_back(point);
	}

	return std::move(average_scan);
}

void lidar_connector::TextFileDriver::Reset() const {
	return;
}

void lidar_connector::TextFileDriver::ParsePointsLine(std::string &line_to_parse,
                                                      std::vector<lidar_shared::Point> *points) const {
	if (points == nullptr) {
		std::cerr << "points must not be null" << std::endl;
		return;
	}

	std::stringstream scan_data(line_to_parse);
	std::string token;
	points->clear();

	while (getline(scan_data, token, this->kPointDelimiter)) {
		(*points).push_back(this->ParsePointString(token));
	}
}

lidar_shared::Point lidar_connector::TextFileDriver::ParsePointString(const std::string &point_string) const {
	lidar_shared::Point parsedPoint;
	constexpr int kMessagePartsCount = 3;

	std::istringstream is(point_string);
	std::string parts[kMessagePartsCount];
	std::string token;

	int i = 0;
	while (getline(is, token, this->kPointAttributesDelimiter)) {
		parts[i] = token;
		++i;
	}

	parsedPoint.angle = std::fmod((float) ::atof(parts[2].c_str()), 360);
	parsedPoint.distance = ((float) ::atof(parts[1].c_str()));
	parsedPoint.quality = (float) ::atof(parts[0].c_str());

	return parsedPoint;
}
