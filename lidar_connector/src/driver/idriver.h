#ifndef LIDAR_CONNECTOR_IDRIVER_H
#define LIDAR_CONNECTOR_IDRIVER_H

#include "model/lidar_device_info.h"
#include "shared/model/point.h"

#include <vector>

namespace lidar_connector {
class IDriver {
public:
  virtual ~IDriver() {};

  /// Connects to the LIDAR device.
  virtual void Connect() = 0;

  /// Obtains a LIDAR device info data.
  ///
  /// \return struct lidar_device_info filled with actual
  ///         device info data
  virtual const lidar_device_info GetDeviceInfo() const = 0;

  /// Flag that signals if a LIDAR device is connected.
  ///
  /// \return true if LIDAR is connected
  virtual const bool IsConnected() const = 0;

  /// Initializes all things that are necessary to start
  /// scanning (start motor...).
  ///
  /// \param motor_pwd motor pwd speed.
  virtual void StartScanning(uint16_t motor_pwd) = 0;

  /// Ends all things related to scanning (stop motor,
  /// turn off laser...).
  virtual void StopScanning() = 0;

  /// Perform one scanning cycle.
  ///
  /// For RPLidar this means to collect all points from
  /// one circle run and to return them in vector.
  ///
  /// \example
  ///
  /// driver->Connect();
  /// driver->StartScanning();
  ///
  /// auto data = driver->GetScanData()
  /// for (auto const &value: data) {
  ///   std::cout << ", Q: " << value.quality;
  ///   std::cout << ", D: " << value.distance;
  ///   std::cout << ", A: " << value.angle << std::endl;
  /// }
  ///
  /// driver->StopScanning();
  ///
  /// \param iterations_to_average number of iterations that should be used for moving average calculation.
  /// \return vector of Points from a single scan.
  virtual std::vector<lidar_shared::Point> GetScanData(int iterations_to_average = 0) = 0;

  /// Resets device's state.
  virtual void Reset() const = 0;
};
}

#endif //LIDAR_CONNECTOR_IDRIVER_H
