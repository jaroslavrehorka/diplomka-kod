#ifndef LIDAR_CONNECTOR_RPLIDAR_H
#define LIDAR_CONNECTOR_RPLIDAR_H

#include <deque>
#include "idriver.h"
#include "rplidar.h"

namespace lidar_connector {

/// Shortcut for SLAMTEC's sdk namespace
namespace rplidar_sdk = rp::standalone::rplidar;

/// This class is an implementation of IDriver interface
/// for Slamtec's RPLidar 2.
class RplidarDriver : public IDriver {
private:
  /// Angle covered by RPlidar is full circle 360°.
  const uint32_t kScannedAngle = 360;

  rplidar_sdk::RPlidarDriver *driver;
  lidar_device_info device_info;
  bool is_scanning;
  std::deque<std::vector<lidar_shared::Point>> past_scans;

  /// Loads info from the LIDAR device and
  /// saves it into device_info field.
  ///
  /// \throws std::runtime_error when:
  ///         - The device is not connected if there is
  ///           a problem with obtaining device info.
  void ParseInfoFromDevice();

  /// Checks status of a LIDAR device.
  ///
  /// \return true if device is ok.
  /// \throws std::runtime_error when:
  ///         - An internal error is detected.
  ///         - cannot obtain health status
  const bool CheckLidarHealth() const;
public:
  RplidarDriver();
  ~RplidarDriver();

  /// \see IDriver.Connect().
  /// \throws std::runtime_error when:
  ///         - Is unable to create driver instance (insufficient memory).
  ///         - Cannot bind specified port (port does not exists,
  ///           is already bound or user does not have rights to access this port).
  void Connect() override;

  /// \see IDriver.GetDeviceInfo().
  const lidar_device_info GetDeviceInfo() const override;

  /// \see IDriver.IsConnected().
  const bool IsConnected() const override;

  /// \see IDriver.StartScanning().
  /// \throws std::runtime_error when:
  ///         - The device is not connected - requires
  ///           to call Connect() before being called.
  ///
  /// \param motor_pwd motor pwd speed.
  void StartScanning(uint16_t motor_pwd) override;

  /// \see IDriver.StopScanning().
  /// \throws std::runtime_error when:
  ///         - The device is not connected - requires
  ///           to call Connect() before being called.
  void StopScanning() override;

  /// \see IDriver.GetScanData().
  /// \throws std::runtime_error when:
  ///         - The device is not connected.
  ///         - The device is not in scanning mode - requires
  ///           to call StartScanning() before being called.
  std::vector<lidar_shared::Point> GetScanData(int iterations_to_average = 0) override;

  /// Calculates average from past scans saved in past_scans attribute.
  ///
  /// \return average scan of n last scans where n is past_scans.size().
  std::vector<lidar_shared::Point> GetAverageScan() const;

  /// Resets device's state.
  void Reset() const override;

};
}

#endif //LIDAR_CONNECTOR_RPLIDAR_H
