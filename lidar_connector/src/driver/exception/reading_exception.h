//
// Created by jrehorka on 4/4/17.
//

#ifndef LIDAR_CONNECTOR_READING_EXCEPTION_H
#define LIDAR_CONNECTOR_READING_EXCEPTION_H

#include <exception.hpp>
#include <stdexcept>
namespace lidar_connector {
class ReadingException : public std::runtime_error {
public:
  ReadingException(const string &__arg) : runtime_error(__arg) {}
  ReadingException(const char *__arg) : runtime_error(__arg) {}
};
}

#endif //LIDAR_CONNECTOR_READING_EXCEPTION_H
