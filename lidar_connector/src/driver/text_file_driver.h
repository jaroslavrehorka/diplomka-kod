//
// Created by jrehorka on 4/4/17.
//

#ifndef LIDAR_CONNECTOR_TEXT_FILE_DRIVER_H
#define LIDAR_CONNECTOR_TEXT_FILE_DRIVER_H

#include "idriver.h"

namespace lidar_connector {

/// Implementation if IDriver for loading LIDAR data from
/// text file.
///
/// Text file format:
///     - each line is one iteration of scan
///     - each point is represented as <quality>,<distance>,<angle>
///     - points are separated with semicolor ;
class TextFileDriver : public IDriver {
private:
  constexpr static char kPointDelimiter = ';';
  constexpr static char kPointAttributesDelimiter = ',';
  const uint32_t kScannedAngle = 360;
  std::string file_path;
  bool rewind_on_finish;
  uint32_t simulated_delay_seconds;
  std::ifstream &infile;
  std::deque<std::vector<lidar_shared::Point>> past_scans;

  lidar_shared::Point ParsePointString(const std::string &point_string) const;
  void ParsePointsLine(std::string &line_to_parse, std::vector<lidar_shared::Point> *points) const;

public:
  /// Creates a new instance of TextFileDriver, simulates real LIDAR device.
  ///
  /// \param file_data_stream file stream with a lidar data.
  /// \param loop_file flag that allows going to the begining of a file when EOF is detected.
  /// \param simulated_delay_seconds delay between returning each line from lidar data file.
  TextFileDriver(std::ifstream &file_data_stream, bool loop_file, uint32_t simulated_delay_seconds);
  void Connect() override;
  const lidar_device_info GetDeviceInfo() const override;
  const bool IsConnected() const override;
  void StartScanning(uint16_t motor_pwd) override;
  void StopScanning() override;

  /// L
  ///
  /// \return
  std::vector<lidar_shared::Point> GetScanData(int iterations_to_average = 0) override;
  void Reset() const override;

  /// Calculates average from past scans saved in past_scans attribute.
  ///
  /// \return average scan of n last scans where n is past_scans.size().
  std::vector<lidar_shared::Point> GetAverageScan() const;
};
}

#endif //LIDAR_CONNECTOR_TEXT_FILE_DRIVER_H
