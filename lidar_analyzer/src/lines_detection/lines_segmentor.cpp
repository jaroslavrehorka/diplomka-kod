//
// Created by jrehorka on 4/5/17.
//

#include "lines_segmentor.h"
#include "line_segment.h"

#include <eigen3/Eigen/Eigen>
#include <cmath>
#include <iostream>
#include <list>

double lidar_analyzer::LinesSegmentor::GetAngleBetweenVectors(lidar_shared::Point &point_a,
                                                              lidar_shared::Point &point_b,
                                                              lidar_shared::Point &peak) const {

	Eigen::Vector2f first_vector(peak.getX() - point_a.getX(), peak.getY() - point_a.getY());
	Eigen::Vector2f second_vector(peak.getX() - point_b.getX(), peak.getY() - point_b.getY());

	double dot_product = first_vector.adjoint() * second_vector;
	auto lenght_product = first_vector.norm() * second_vector.norm();

	if (lenght_product == 0) {
		return 0;
	}

	auto angle = acos(std::min(std::max(dot_product / lenght_product, -1.0), 1.0));

	return angle;
}

void lidar_analyzer::LinesSegmentor::FindSubStrings(std::vector<std::list<lidar_shared::Point>> *list_of_sub_strings_out) {
	if (list_of_sub_strings_out == nullptr) {
		std::cerr << "list_of_sub_strings_out must not be null" << std::endl;
		return;
	}

	std::list<lidar_analyzer::LineSegment> final_segments;

	for (auto &points_string : *list_of_sub_strings_out) {
		if (points_string.size() < 4) {
			continue;
		}

		final_segments.splice(final_segments.end(), this->AnalyzeLineSegment(points_string));
	}

	list_of_sub_strings_out->clear();
	for (auto &segment: final_segments) {
		list_of_sub_strings_out->push_back(segment.line_segment);
	}
}

std::list<lidar_analyzer::LineSegment> lidar_analyzer::LinesSegmentor::AnalyzeLineSegment(
	std::list<lidar_shared::Point> &single_segment) {

	std::list<lidar_shared::Point>::iterator current_iter;
	double biggest_change_distance = -1;
	double segment_significance = 0;
	float deviation_total = 0;

	for (auto it = single_segment.begin(); it != single_segment.end(); ++it) {

		if (it != single_segment.begin() && (*it) != single_segment.back()) {
			auto previous_point = std::prev(it);
			auto next_point = std::next(it);
			it->direction_change_angle = M_PI - this->GetAngleBetweenVectors(*previous_point, *next_point, *it);
			deviation_total += it->GetStringChangeAngleDeg();
		} else {
			it->direction_change_angle = 0;
		}

		auto distance =
			lidar_shared::Point::GetDistanceFromEndPointsLine(single_segment.front(), single_segment.back(), (*it));

		if (distance > biggest_change_distance
			&& it
				!= single_segment.begin()) { // We don't want first element, because splice would split list into empty and full sublist
			current_iter = it;
			biggest_change_distance = distance;
			segment_significance = distance == 0 ? 0 :
			                       lidar_shared::Point::GetDistanceBetweenPoints(single_segment.front(),
			                                                                     single_segment.back())
				                       / distance;
		}
	}

	std::list<lidar_shared::Point> divided_segment;
	std::list<lidar_analyzer::LineSegment> segment_list_return;
	std::list<lidar_analyzer::LineSegment> child_sub_segments;
	bool any_child_more_significant = false;

	if (single_segment.size() > this->kMinimalSegmentSize) {
		divided_segment.splice(divided_segment.begin(), single_segment, current_iter, single_segment.end());

		child_sub_segments = this->AnalyzeLineSegment(single_segment);
		child_sub_segments.splice(child_sub_segments.end(), this->AnalyzeLineSegment(divided_segment));

		for (auto &sub_segment: child_sub_segments) {
			if (sub_segment.significance > segment_significance) {
				segment_list_return = child_sub_segments;
				any_child_more_significant = true;

				break;
			}
		}

		if (!any_child_more_significant) {
			lidar_analyzer::LineSegment out_merged_segment;
			out_merged_segment.significance = segment_significance;

			for (auto &sub_segment: child_sub_segments) {
				out_merged_segment.line_segment.splice(out_merged_segment.line_segment.end(), sub_segment.line_segment);
			}

			segment_list_return.push_back(out_merged_segment);
		}

	} else {
		auto bottom_segment = lidar_analyzer::LineSegment();
		bottom_segment.significance = segment_significance;
		bottom_segment.line_segment = single_segment;

		segment_list_return.push_back(bottom_segment);
	}

	return segment_list_return;
}
