//
// Created by jrehorka on 4/4/17.
//

#ifndef LIDAR_ANALYZER_LINES_DETECTOR_H
#define LIDAR_ANALYZER_LINES_DETECTOR_H

#include "shared/model/line.h"
#include "shared/model/point.h"

#include <vector>
#include <list>

namespace lidar_analyzer {
class LinesDetector {
private:
  const static int kMaximalDeviance = 5;
  const static int kMinimalLineSize = 3;

  void DetectLines(std::vector <std::list<lidar_shared::Point>> &point_strings,
                   std::vector <lidar_shared::Line> *lines_data_out);
public:
  void AnalyzeData(std::vector <lidar_shared::Point> *scan_data_out,
                   std::vector <lidar_shared::Line> *lines_data_out);
};
}

#endif //LIDAR_ANALYZER_LINES_DETECTOR_H
