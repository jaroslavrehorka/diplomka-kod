//
// Created by jrehorka on 4/3/17.
//

#include "core/socket_control/socket_control.h"
#include "bootstrap.h"
#include "lines_detection/lines_detector.h"

#include <csignal>

bool lidar_analyzer::Bootstrap::ctrl_c_pressed = false;

void lidar_analyzer::Bootstrap::StopSignal(int) {
	ctrl_c_pressed = true;
}

lidar_analyzer::Bootstrap::Bootstrap(const lidar_analyzer::Options run_options)
	: run_options(run_options) {}

int lidar_analyzer::Bootstrap::Run() {

	auto return_status = EXIT_SUCCESS;
	auto socket_control = std::make_unique<lidar_analyzer::SocketControl>(run_options);
	try {
		socket_control->Connect();

		std::signal(SIGINT, lidar_analyzer::Bootstrap::StopSignal);

		std::vector<lidar_shared::Point> points;
		std::vector<lidar_shared::Line> lines;
		auto lines_detector = lidar_analyzer::LinesDetector();

		while (true) {
			socket_control->ReadData(&points);
			lines_detector.AnalyzeData(&points, &lines);

			if (!this->run_options.quiet) {
				for (auto const point : points) {
					std::cout << "Distance:" << point.distance << "Quality:" << point.quality << "Angle: "
					          << point.angle << " x: " << point.getX() << " y: " << point.getY() << std::endl;
					std::cout.flush();
				}
			}

			socket_control->SendData(points, lines);

			if (ctrl_c_pressed) {
				break;
			}

			std::this_thread::sleep_for(std::chrono::microseconds(200));
		}

	} catch (std::exception &er) {
		std::cerr << er.what() << std::endl;
		return_status = EXIT_FAILURE;
	} catch (libsocket::socket_exception &er) {
		std::cerr << "Socket exception" << er.mesg << std::endl;
		return_status = EXIT_FAILURE;
	} catch (...) {
		std::cerr << "Unknown exception exception" << std::endl;
		return_status = EXIT_FAILURE;
	}

	return return_status;
}
