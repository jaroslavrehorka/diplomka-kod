//
// Created by jrehorka on 4/3/17.
//

#ifndef LIDAR_ANALYZER_BOOTSTRAP_H
#define LIDAR_ANALYZER_BOOTSTRAP_H

#include "model/options.h"

namespace lidar_analyzer {

/// Main activation class that handles all required
/// activities for starting up the program.
class Bootstrap {
private:
  lidar_analyzer::Options run_options;
  static bool ctrl_c_pressed;
  static void StopSignal(int);

public:
  Bootstrap(const lidar_analyzer::Options run_options);

  /// Main method that runs the program.
  int Run();
};
}

#endif //LIDAR_ANALYZER_BOOTSTRAP_H
