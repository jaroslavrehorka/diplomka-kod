#include "core/bootstrap.h"

#include <iostream>
#include <getopt.h>

int parse_args(int argc, char *argv[], lidar_analyzer::Options *options) {
	int c;
	const char *short_opt = "hsqp:a:e:s:";
	struct option long_opt[] =
		{
			{"help", no_argument, NULL, 'h'},
			{"verbose", no_argument, NULL, 'v'},
			{"quiet", no_argument, NULL, 'q'},
			{"port", required_argument, NULL, 'p'},
			{"address", required_argument, NULL, 'a'},
			{"server-port", required_argument, NULL, 'e'},
			{"server-address", required_argument, NULL, 's'},
			{NULL, 0, NULL, 0}
		};

	while ((c = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1) {
		switch (c) {
		case -1:       /* no more arguments */
		case 0:        /* long options toggles */
			break;
		case 'a': options->sockets_host_address = optarg;
			break;
		case 'p': options->sockets_host_port = optarg;
			break;
		case 's': options->sockets_server_address = optarg;
			break;
		case 'e': options->sockets_server_port = optarg;
			break;
		case 'v': options->verbose = true;
			break;
		case 'q': options->quiet = true;
			break;
		case 'h': std::cout << "Usage: " << argv[0] << " [OPTIONS]" << std::endl;
			std::cout
				<< "  -p, --port=<port>               Port number that will be used as port for socket host"
				<< std::endl;
			std::cout
				<< "  -a, --address=<address>         Address that will be used as an address for socket host,"
				<< std::endl
				<< "                                  if the port is not provided sockets won't be used, "
				<< std::endl
				<< "                                  if the address is not provided \"localhost\" will be used"
				<< std::endl;
			std::cout
				<< "  -e, --server-port=<port>        Same as a -p but used for connecting to remote server"
				<< std::endl;
			std::cout
				<< "  -s, --server-address=<port>     Same as a -a but used for connecting to remote server"
				<< std::endl;
			std::cout << "  -v, --verbose                   Whether more verbose output should be used." << std::endl;
			std::cout
				<< "  -q, --quiet                     Whether quiet output mode should be used, has higher priority than verbose."
				<< std::endl;
			std::cout << "  -h, --help                      Print this help and exit" << std::endl;
			std::cout << std::endl;
			return -1;
		default: std::cerr << argv[0] << ": invalid option -- " << c << std::endl;
			std::cerr << "Try `" << argv[0] << " --help' for more information." << std::endl;
			return 1;
		};
	};

	return 0;
}

int main(int argc, char *argv[]) {
	lidar_analyzer::Options options;
	auto parse_result = parse_args(argc, argv, &options);

	if (parse_result == 1) {
		return EXIT_FAILURE;
	} else if (parse_result == -1) {
		return EXIT_SUCCESS;
	}

	lidar_analyzer::Bootstrap bootstrap(options);
	return bootstrap.Run();
}